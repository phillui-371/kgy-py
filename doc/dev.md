# Development Manual

## Procedure
1. Write test case first to illustrate what the module/function is, i.e. input/output
    - MUST have src check to ensure all function do not modify src at all
2. Base on test case, develop target functions
    - function as small as possible
    - keep function single prupose
3. Run test case to ensure implementation works well

## Debug
0. Open issue on GitLab, branch with issue tag
    - tag will be created by main contributor
1. Write a new test to demonstrate the issue
    - NEVER edit existing test unless test case itself has issues
        - if existing test case needed to be edited, post issue first, if contributors find that it is necessary, branch -> fix -> PR
2. Fix code implementation
3. Run all test
    - pass -> ship it! Make PR and have an afternoon tea!
    - fail -> back to 2