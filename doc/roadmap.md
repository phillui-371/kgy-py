## Features
- Multiprocess/Async support hint(by type)
- all pure, which mean can be tested

## Tasks
- implement fundamentel Typeclass (~Early July)
- implement persistance data structure (~Late August)
    - implement Typeclass on data structure
- implement utility functions (will be added if necessary, no deadline)