import operator

from KGYPy import Right, Left, Either, curry, liftA2
from basic_type import Unit


def test_Right():
    ins = Right(1)
    assert ins.getValue() == 1
    assert ins.is_right
    assert not ins.is_left
    assert next(iter(ins)) == 1

    # Monad
    assert ins >> (lambda x: Right(x+1)) == Right(2)
    assert ins.bind(lambda x: Right(x+1)) == Right(2)
    assert ins >> (lambda _: Right(3)) == Right(3)
    assert ins.bind(lambda _: Right(3)) == Right(3)

    # Alternative
    assert ins | Left(None) == ins
    assert ins | Either.empty() == ins
    assert Either.empty() == Right(Unit)
    # assert Right(...).some()
    # assert Right(...).many()

    # functor and applicative
    f = curry(operator.add)
    assert f * Right(1) & Right(2) == Right(3)
    assert liftA2(f, Right(1), Right(2)) == Right(3)
    assert Right(1).fmap(f).amap(Right(2)) == Right(3)
    assert Right(1).replace(2) == Right(2)
    assert Right(1).consume_self(Right(2)) == Right(2)
    assert Right(1).consume_other(Right(2)) == Right(1)
