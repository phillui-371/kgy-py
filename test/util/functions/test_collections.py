from KGYPy import idx, fst, snd, head, tail, init, last, take_until, take_while, skip_until, skip_while, split_by


def test_idx():
    ls = list(range(10))

    assert idx(0, ls) == 0
    assert idx(1, ls) == 1
    assert fst(ls) == ls[0]
    assert snd(ls) == ls[1]
    assert head(ls) == ls[0]
    assert ls[1:] == tail(ls)
    assert ls[:-1] == init(ls)
    assert ls[-1] == last(ls)


def test_take():
    ls = list(range(10))
    expect = list(range(6))

    assert list(take_until(lambda x: x > 5, ls)) == expect
    assert list(take_until(lambda x: x > 5)(ls)) == expect

    assert list(take_while(lambda x: x <= 5)(ls)) == expect
    assert list(take_while(lambda x: x <= 5, ls)) == expect


def test_skip():
    ls = list(range(10))
    expect = list(range(6, 10))

    assert list(skip_until(lambda x: x > 5, ls)) == expect
    assert list(skip_until(lambda x: x > 5)(ls)) == expect

    assert list(skip_while(lambda x: x <= 5)(ls)) == expect
    assert list(skip_while(lambda x: x <= 5, ls)) == expect


def test_split_by():
    ls = list(range(10))
    expect = [[x] for x in range(0, 10, 2)]

    assert list(split_by(lambda x: x & 1 == 1, ls)) == expect
    assert list(split_by(lambda x: x & 1 == 1)(ls)) == expect
