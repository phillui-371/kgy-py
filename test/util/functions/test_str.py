from util.functions.str_ import remove


def test_remove():
    assert remove('foo bar'.split(), 'foo is bar') == ' is '
    assert remove('foo bar'.split())('Foo bar 2000') == 'Foo  2000'