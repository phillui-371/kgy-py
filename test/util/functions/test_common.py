from KGYPy import id_, const, Unit, Y


def test_id_():
    assert id_(1) == 1
    assert id_(None) is None
    assert id_(lambda x: x) != (lambda x: x)
    assert id_(id_) == id_

    class Test: pass

    assert id_(Test) == Test
    assert id_(Test()) != Test()

def test_const():
    assert const(1, 2) == 1
    assert const(None, int) is None
    f = const(Unit)
    assert f(1) == Unit
    assert f(None) == Unit


def test_Y():
    fac = lambda f: lambda n: (1 if n<2 else n*f(n-1))
    assert Y(fac)(9) == 362880
    fib = lambda f: lambda n: 0 if n == 0 else (1 if n == 1 else f(n-1) + f(n-2))
    assert Y(fib)(9) == 34
