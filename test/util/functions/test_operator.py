from collections import OrderedDict

import pytest
from util.functions.operator import *
from util.functions.common import flip


# Compare
def test_lt():
    is_pos = c_lt(0)
    assert is_pos(1)
    assert not is_pos(0)
    assert not is_pos(-1)
    assert c_lt(0, 1)
    assert not c_lt(0, 0)
    assert not c_lt(0, -1)


def test_le():
    is_not_neg = c_le(0)
    assert is_not_neg(1)
    assert is_not_neg(0)
    assert not is_not_neg(-1)
    assert c_le(0, 1)
    assert c_le(0, 0)
    assert not c_le(0, -1)


def test_eq():
    is_eq = c_eq(0)
    assert is_eq(0)
    assert not is_eq(1)
    assert not is_eq(None)  # this will be warned by pylint/pycharm, although it can be execute normally


def test_ne():
    is_ne = c_ne(0)
    assert is_ne(1)
    assert not is_ne(0)
    assert is_ne(None)


def test_ge():
    is_not_pos = c_ge(0)
    assert is_not_pos(-1)
    assert is_not_pos(0)
    assert not is_not_pos(1)
    assert c_ge(0, -1)
    assert c_ge(0, 0)
    assert not c_ge(0, 1)


def test_gt():
    is_neg = c_gt(0)
    assert is_neg(-1)
    assert not is_neg(0)
    assert not is_neg(1)
    assert c_gt(0, -1)
    assert not c_gt(0, 0)
    assert not c_gt(0, 1)


# logical
def test_is():
    is1 = c_is(1)
    assert is1(1)
    assert not is1(None)
    assert not is1(1.)

    class Test: pass

    class Test2(Test): pass

    is_cls = c_is(Test)
    assert is_cls(Test)
    assert not is_cls(Test2)
    assert not is_cls(Test())

    ins = Test()
    is_ins = c_is(ins)
    assert is_ins(ins)
    assert not is_ins(Test())


def test_is_not():
    is_not1 = c_is_not(1)
    assert not is_not1(1)
    assert is_not1(None)
    assert is_not1(1.)

    class Test: pass

    class Test2(Test): pass

    is_not_cls = c_is_not(Test)
    assert not is_not_cls(Test)
    assert is_not_cls(Test2)
    assert is_not_cls(Test())

    ins = Test()
    is_not_ins = c_is_not(ins)
    assert not is_not_ins(ins)
    assert is_not_ins(Test())


# math
def test_add():
    add1 = c_add(1)
    assert add1(1) == 2
    assert add1(1j) == 1 + 1j
    assert add1(.1) == 1.1

    add_ls = c_add([10])
    assert add_ls([]) == [10]
    assert add_ls([2]) == [10, 2]

    raw = [10]
    add_ls2 = c_add(raw)
    assert add_ls2([1,2]) == [10,1,2]
    assert raw == raw


def test_floordiv():
    d5 = flip(c_floordiv, 5)
    assert d5(5) == 1
    assert d5(8) == 1
    assert d5(0) == 0


def test_mod():
    mod7 = flip(c_mod, 7)
    assert mod7(11) == 4


def test_mul():
    mul3 = c_mul(3)
    assert mul3(5) == 15
    assert mul3(2.5) == 7.5


def test_matmul():
    class Test:
        def __init__(self, v):
            self.v = v

        def __matmul__(self, other):
            return (self.v - other.v) / self.v

    assert Test(5) @ Test(2) == 3 / 5
    assert c_matmul(Test(5))(Test(2)) == 3 / 5


def test_pow():
    sq = flip(c_pow, 2)
    assert sq(3) == 9
    assert (.01 - sq(.1)) / .01 < 1e-5
    assert sq(1j) == -1


def test_sub():
    sub9 = flip(c_sub, 9)
    assert sub9(9) == 0
    assert sub9(0) == -9


def test_truediv():
    d5 = flip(c_truediv, 5)
    assert d5(5) == 1
    assert d5(0) == 0
    assert d5(9) == 9 / 5


# bit
def test_and():
    odd = c_and(1)
    assert odd(5) == 1
    assert odd(2) == 0

    even = lambda x: not odd(x)
    assert not even(5)
    assert even(4)


def test_lshift():
    lsh1 = c_lshift(1)
    assert lsh1(2) == 2 << 1


def test_rshift():
    rsh1 = c_rshift(1)
    assert rsh1(2) == 2 >> 1


def test_or():
    or5 = c_or(0b101)
    assert or5(0b10) == 7
    assert or5(0b1000) == 13


def test_xor():
    xor2 = c_xor(2)
    assert xor2(5) == 5 ^ 2
    assert xor2(xor2(7)) == 7


# sequence
def test_contains():
    has_1 = flip(c_contains, 1)
    assert has_1({1: 'test'})
    assert has_1([1])
    assert has_1({1})
    assert has_1((1,))
    assert not has_1({})
    assert not has_1([])
    assert not has_1(set())
    assert not has_1(())

def test_count_of():
    count9 = flip(c_count_of, 9)
    assert count9([9,9]) == 2
    assert count9((9,9,9)) == 3
    assert count9({9: 1}) == 1
    assert count9({9,9}) == 1
    assert count9('9999') == 0

def test_delitem():
    del0 = flip(c_delitem, 0)
    raw1 = [1,2,3]
    del0(raw1)
    assert raw1 == [2,3]

def test_getitem():
    get0 = flip(c_getitem, 0)
    assert get0([1,2,3]) == 1
    assert get0(OrderedDict([(0, 't'),(1, 'x')])) == 't'
    assert get0('abc') == 'a'

    get_a = flip(c_getitem, 'a')
    assert get_a(dict(a=1,b=2)) == 1

def test_index_of():
    check1 = flip(c_getitem, 1)
    assert check1([3,2,1]) == 2

# inplace
def test_iadd():
    add_ls1 = flip(c_iadd, [1])
    raw1 = [2,3]
    assert add_ls1(raw1) == [2,3,1]
    assert raw1 != [2,3]

    add1 = flip(c_iadd, 1)
    raw1 = 2
    assert add1(raw1) == 3  # primitive type will trigger copy
    assert raw1 == 2

def test_iand():
    and1 = flip(c_iand, 1)
    raw1 = 10
    assert and1(raw1) == 0
    assert raw1 == 10

def test_ifloordiv():
    d5 = flip(c_ifloordiv, 5)
    raw1 = 10
    assert d5(raw1) == 2
    assert raw1 == 10

def test_ilshift():
    lsh2 = flip(c_ilshift, 2)
    raw1 = 8
    assert lsh2(raw1) == 8 << 2
    assert raw1 == 8

def test_imod():
    mod3 = flip(c_imod, 3)
    raw1 = 7
    assert mod3(raw1) == 1
    assert raw1 == 7

def test_imul():
    mul2 = flip(c_imul, 2)
    raw1 = 3
    assert mul2(raw1) == 3 * 2
    assert raw1 == 3

def test_imatmul():
    class Test:
        def __init__(self, v):
            self.v = v

        def __matmul__(self, other):
            return (self.v - other.v) / self.v

    raw1 = Test(2)
    m2 = flip(c_imatmul, raw1)
    raw2 = Test(3)
    assert m2(raw2) == 1/3
    assert raw2.v == 3
    assert raw1.v == 2

def test_ior():
    or2 = flip(c_ior, 2)
    raw1 = 4
    assert or2(raw1) == 4 | 2
    assert raw1 == 4

def test_ipow():
    sq = flip(c_ipow, 2)
    raw1 = 1j
    assert sq(raw1) == -1
    assert raw1 == 1j

def test_irshift():
    rsh2 = flip(c_irshift, 2)
    raw1 = 10
    assert rsh2(raw1) == 10 >> 2
    assert raw1 == 10

def test_isub():
    sub2 = flip(c_isub, 2)
    raw1 = 3
    assert sub2(raw1) == 1
    assert raw1 == 3

def test_itruediv():
    d2 = flip(c_itruediv, 2)
    raw1 = 3
    assert d2(raw1) == 3 / 2
    assert raw1 == 3

def test_ixor():
    xor2 = flip(c_ixor, 2)
    raw1 = 3
    assert xor2(raw1) == 3 ^ 2
    assert raw1 == 3