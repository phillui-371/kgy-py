from util.curry_ import curry


def g_test():
    pass


def test_function():
    def test():
        pass


def test_lambda():
    pass


def test_method():
    class Test:
        def __init__(self, v):
            self.v = v

        def a(self, v):
            return self.v + v

        @classmethod
        def b(cls, v):
            return cls(v)

        def __eq__(self, other):
            return self.v == other.v

    a = curry(Test(3).a)
    assert a(2) == 5
    b = curry(Test.b)
    assert b(5) == Test(5)


def test_cls():
    # cls new/init/call
    pass


def test_builtin():
    f = curry(map)
    m_add1 = f(lambda x: x + 1)
    assert list(m_add1([1, 2, 3, 4])) == [2, 3, 4, 5]

    f = curry(filter)
    even = f(lambda x: x & 1 == 0)
    assert list(even([1, 2, 3, 4])) == [2, 4]

    f = curry(pow)
    sq = lambda base: f(base, 2)
    assert sq(4) == 16


def test_varargs_kwargs():
    pass


def test_multiprocess():
    pass


def test_async():
    pass


def test_reuse():
    pass
