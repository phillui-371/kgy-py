from __future__ import annotations

from dataclasses import dataclass
from typing import TypeVar, Callable, Generic, Tuple, Sequence
from operator import eq
import re

from KGYPy import Maybe, Just, Nothing, Monad, Alternative, const, Unit, trace, Functor, Applicative, snd
from struct_ import JSON, JNull, JBool, JString, JNumber, JArray, JObject
import inspect

_T = TypeVar('_T')
_U = TypeVar('_U')
_R = TypeVar('_R')

@dataclass
class Parser(Monad[_T], Alternative[_T], Generic[_T]):
    f: Callable[[str], Maybe[Tuple[_T, str]]]

    def parse(self, raw: str) -> Maybe[Tuple[_T, str]]: return self.f(raw)
    __call__ = parse

    @classmethod
    def unit(cls, value: _T) -> Functor[_T]:
        return cls(value)

    def bind(self, function: Callable[[_T], Parser[_U]]) -> Parser[_U]:
        def core(s: str):
            _ = self(s)
            if _.is_nothing:
                return _
            m, r = _.getValue()
            return function(m)(r)
        return Parser(core)

    @classmethod
    def return_(cls, value: _T) -> Parser[_T]:
        return cls(lambda x: Just((value, x)))

    def fmap(self, fn: Callable[[_T], _R]) -> Functor[_R]:
        def core(s: str):
            _ = self.parse(s)
            if _.is_nothing:
                return _
            m, r = _.getValue()
            return Just((fn(m), r))
        return Parser(core)

    def amap(self, value: Applicative[_T]) -> Applicative[_U]:
        def core(s: str):
            _ = self.parse(s)
            if _.is_nothing:
                return _
            f, r = _.getValue()
            return value.fmap(f).parse(r)
        return Parser(core)

    @classmethod
    def empty(cls) -> Alternative[_T]: return Parser(lambda x: const(Nothing, x))

    def __or__(self, value: Alternative[_T]) -> Alternative[_T]:
        def core(s: str):
            _ = self.parse(s)
            if _.is_nothing:
                return value(s)
            return _
        return Parser(core)

def satisfy(f: Callable[[str], bool]) -> Parser[Sequence[_T]]:
    def core(x: str) -> Maybe[Tuple[Sequence[_T], str]]:
        if not x:
            return Nothing
        y, ys = x[0], x[1:]
        if f(y):
            return Just((y, ys))
        return Nothing
    return Parser(core)

def is_hex(s: str) -> bool: return bool(re.search(r'^[0-9a-fA-F]$', s))
def is_oct(s: str) -> bool: return bool(re.search(r'^[0-7]$', s))


# consume single char
p_char: Callable[[str], Parser[str]] = lambda c: satisfy(lambda x: eq(c, x))
p_space: Parser[str] = satisfy(lambda _: _.isspace())
p_digit: Parser[str] = satisfy(lambda _: _.isdigit())
p_lower: Parser[str] = satisfy(lambda _: _.islower())
p_upper: Parser[str] = satisfy(lambda _: _.isupper())
p_alpha: Parser[str] = satisfy(lambda _: _.isalpha())
p_alphanum: Parser[str] = satisfy(lambda _: _.isalnum())
p_hex: Parser[str] = satisfy(is_hex)
p_oct: Parser[str] = satisfy(is_oct)
p_one_of: Callable[[str], Parser[str]] = lambda cs: satisfy(lambda _: _ in cs)
p_none_of: Callable[[str], Parser[str]] = lambda cs: satisfy(lambda _: _ not in cs)

def p_str(s: str) -> Parser[str]:
    if not s:
        return Parser.return_("")
    x, xs = s[0], s[1:]
    return p_char(x) >> (
        lambda c: p_str(xs) >> (
            lambda cs: Parser.return_(f"{c}{cs}")
        )
    )

def many(p: Parser[_T]) -> Parser[Sequence[_T]]:
    def core(ss: str):
        _ = p(ss)
        if _.is_nothing:
            return Just(('', ss))
        return _ >> (lambda x: core(x[1]) >> (lambda y: Just((x[0] + y[0], y[1]))))
    return Parser(core)

def some(p: Parser[_T]) -> Parser[Sequence[_T]]:
    def core(ss: str):
        _ = p(ss)
        if _.is_nothing:
            return _
        # TODO
        return _ >> (lambda x: core(x[1]) >> (lambda y: Just((x[0] + y[0], y[1]))))
    return Parser(core)

def skip_many(a: Parser[_T]) -> Parser[Unit]:
    return many(a).bind_right(Parser.return_(Unit))

def skip_some(a: Parser[_T]) -> Parser[Unit]:
    def core(ss: str):
        _ = a(ss)
        if _.is_nothing:
            return _
        return core(snd(_.getValue()))
    return Parser(core)

def between(lhs: Parser[TypeVar('LHS')], content: Parser[_T], rhs: Parser[TypeVar('RHS')]) -> Parser[_T]:
    return lhs.consume_self(content).consume_other(rhs)

if __name__ == '__main__':
    print(skip_many(p_space).bind_right(p_char('a'))('    a'))