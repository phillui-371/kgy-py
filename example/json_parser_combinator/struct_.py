from abc import ABC
from dataclasses import dataclass
from typing import Union, MutableSequence, MutableMapping

from KGYPy import concat_map


class JSON(ABC): pass


class JNull(JSON): pass


@dataclass
class JBool(JSON):
    v: bool


@dataclass
class JString(JSON):
    v: str


@dataclass
class JNumber(JSON):
    v: Union[int, float]


@dataclass
class JArray(JSON):
    v: MutableSequence[JSON]


@dataclass
class JObject(JSON):
    v: MutableMapping[str, JSON]


def show(obj: JSON) -> str:
    return {
        JNull: "null",
        JBool: "true" if obj.v else "false",
        JString: str_handling(obj.v),
        JNumber: str(obj.v),
        JArray: f"[{','.join(show(obj.v))}]",
        JObject: "{" + ','.join(f"{k}:{show(v)}" for k, v in obj.v.items()) + "}"
    }[type(obj)]


def str_handling(s: str) -> str:
    def core(c: str) -> str:
        try:
            return {
                "'": "'",
                "\"": "\\\"",
                "\\": "\\\\",
                "/": "\\/",
                "\b": "\\b",
                "\f": "\\f",
                "\n": "\\n",
                "\r": "\\r",
                "\t": "\\t",
            }[c]
        except KeyError:
            if c in {r"\{}".format(x) for x in range(32)}:
                return "\\u" + hex(ord(c))[2:].zfill(4)
            return c

    return f"\"{concat_map(core, s)}\""
