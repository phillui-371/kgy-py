# KGY-Py(Kaguya Python)
## Introduction
This lib aims to simulate Haskell and provide some infrastructure like persistent data structure and Optics to let development smooth.

## Module
- [Dataclass](src/KGYPy/dataclass)
    - Common `data` of Haskell including
        - [Either](src/KGYPy/dataclass/either_.py)
        - [Maybe](src/KGYPy/dataclass/maybe_.py)
        - [State](src/KGYPy/dataclass/state.py)
- [error](src/KGYPy/error)
    - error handling facility
- [Persistence](src/KGYPy/persistence)
    - Persistent data structure including
        - PVec
        - PMap
        - PSet
- [Transient](src/KGYPy/transient)
    - Transient data structure including
        - TVec
        - TMap
        - TSet
- [Typeclass](src/KGYPy/typeclass)
    - `class` in Haskell, implemented as `Protocol` of Python
        - [Alternative](src/KGYPy/typeclass/alternative.py)
        - [Applicative](src/KGYPy/typeclass/applicative.py)
        - [Bifoldable](src/KGYPy/typeclass/bifoldable.py)
        - [Bifunctor](src/KGYPy/typeclass/bifunctor.py)
        - [Bitraversable](src/KGYPy/typeclass/bitraversable.py)
        - [Foldable](src/KGYPy/typeclass/foldable.py)
        - [Functor](src/KGYPy/typeclass/functor.py)
        - [Monad](src/KGYPy/typeclass/monad.py)
        - [MonadFail](src/KGYPy/typeclass/monad_fail.py)
        - [MonadFix](src/KGYPy/typeclass/monad_fix.py)
        - [MonadPlus](src/KGYPy/typeclass/monad_plus.py)
        - [MonadZip](src/KGYPy/typeclass/monad_zip.py)
        - [Monoid](src/KGYPy/typeclass/monoid.py)
        - [SemiGroup](src/KGYPy/typeclass/semigroup.py)
        - [Serializable](src/KGYPy/typeclass/serializable.py)
        - [Traversable](src/KGYPy/typeclass/traversable.py)
- [Utility](src/KGYPy/util)
    - utilities that helps development

## Contribution
Procedure to contribute
1. Create issues
2. Fork
3. Pull Request

### Pull Request requirement
1. Source code
2. Test case
    - as typeclass is implemented as protocol, please also provide relevant dataclass with test case
    - for bug fix PR, please add failed situation as new test case
3. Simple documentation of source code
4. Python stub (*.pyi) for type annotation
    - **Optional** if type annotation is added in source code