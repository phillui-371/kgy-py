# from typing import Callable, TypeVar, Tuple, Generic
# from dataclasses import dataclass
#
# T = TypeVar('T')
#
# @dataclass
# class Parser(Generic[T]):
#     P: Callable[[str], Tuple[T, str]]
#
# def parse(p: Parser[T]) -> Callable[[str], Tuple[T, str]]:
#     return p.p
#
# def item() -> Parser[str]:
#     return Parser(lambda xs: [] if len(xs) == 0 else [(xs[0], xs[1:])])
#
# def fmap_p(p: Parser[T], f: Callable):
#     def core(inp: str) -> Tuple[T, str]:
#         _ = parse(p)(inp)
#         if not _:
#             return []
#         else:
#             return [(f(_[0]), _[1])]
#     return Parser(core)
#
# import operator
# f_ = lambda f: lambda: f()
# f3 = f_(f_(f_(operator.add)))
#
# import dill
#
# dill.loads(dill.dumps(lambda x: x + 1))(1)
# dill.dumps(f3)
#
# from pyrsistent import PVector, PMap, PSet, freeze, pmap, pvector, pset
# import random
# import timeit
# from functools import reduce
#
# LOOP = 1000
# MAX = int(1E6)
#
# def list1(t = list):
#     a = t()
#     [a.append(_) for _ in range(MAX)]
#
#
# print(timeit.timeit(lambda: list1(), number=LOOP)/LOOP)
# print(timeit.timeit(lambda: list1(pvector), number=LOOP)/LOOP)
