from setuptools import setup, find_packages

NAME = 'KGY-Py'
VERSION = '0.1'
EXCLUDE_PATH = [
    "*.egg-info",
    "dist"
]
DEPENDENCY = [
    'dill',
    'pathos',
]

if __name__ == '__main__':
    setup(
        name=NAME,
        version=VERSION,
        python_requires='>=3.8',
        packages=find_packages(exclude=EXCLUDE_PATH),
        requires=DEPENDENCY
    )