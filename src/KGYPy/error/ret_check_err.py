from __future__ import annotations
from typing import Type, Callable, TypeVar
from functools import wraps

_T = TypeVar('_T')


class ReturnCheckError(RuntimeError):
    @classmethod
    def default(cls, expect_type: Type, cls_type: Type, fn_name: str) -> ReturnCheckError:
        return cls(f"Return type {cls_type}.{fn_name} is not {expect_type}")


def ret_check(ret_type: Type = None, self_ref: bool = False):
    if not ret_type and not self_ref:
        raise RuntimeError("Either ret_type or self_ref")

    def wrapper(f: Callable):

        @wraps(f)
        def core(*args, **kwargs):
            ret = f(*args, **kwargs)
            target_t = ret_type or type(args[0])
            if not isinstance(f, target_t):
                raise ReturnCheckError.default(target_t, type(args[0]), f.__name__)
            return ret

        return core

    return wrapper


def simple_ret_check(ret_type: Type[_T]):
    def core(result: _T) -> _T:
        if isinstance(result, ret_type):
            return result
        raise ReturnCheckError(f"Result type is {type(result)} instead of expectation {ret_type}")

    return core


__all__ = [
'ReturnCheckError', 'simple_ret_check', 'ret_check'
]