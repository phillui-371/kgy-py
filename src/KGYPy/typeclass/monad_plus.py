from typing import TypeVar, runtime_checkable, Protocol

import typeclass.alternative as alt
import typeclass.monad as m

_T = TypeVar('_T')
_M_co = TypeVar('_M_co', bound='MonadPlus', covariant=True)

@runtime_checkable
class MonadPlus(m.Monad[_T], alt.Alternative[_T], Protocol[_T]):
    @classmethod
    def mzero(cls) -> _M_co:
        return cls.empty()

    def mplus(self, value: _M_co) -> _M_co:
        return self | value


__all__ = [
'MonadPlus'
]