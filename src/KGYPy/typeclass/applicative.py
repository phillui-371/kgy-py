from typing import TypeVar, Callable, Protocol, runtime_checkable

import typeclass.functor as f

_T = TypeVar('_T')
_U = TypeVar('_U')
_R = TypeVar('_R')
_A_co = TypeVar('_A_co', bound='Applicative', covariant=True)

@runtime_checkable
class Applicative(f.Functor[_T], Protocol[_T]):
    def amap(self, value:_A_co) -> _A_co:
        ...

    """
    alias of <*> in Haskell

    Applicative f => f fn & f a === f fn <*> f a
    """

    def __and__(self, value: _A_co) -> _A_co:
        return self.amap(value)

    @classmethod
    def pure(cls, value: _T) -> _A_co:
        return cls.unit(value)

    def consume_self(self, value: _A_co) -> _A_co:
        """
        *> in Haskell

        act as `self *> value`
        """
        import util.functions.common as cm
        return self.replace(cm.id_).amap(value)

    def consume_other(self, value: _A_co) -> _A_co:
        """
        <* in Haskell

        act as `self <* value`
        """
        import util.functions.common as cm
        return liftA2(cm.const, self, value)


def liftA2(f: Callable[[_T, _U], _R], value1: _A_co, value2: _A_co) -> _A_co:
    """ liftA2 f x y = f <$> x <*> y"""
    return value1.fmap(f).amap(value2)


__all__ = [
'Applicative', 'liftA2'
]