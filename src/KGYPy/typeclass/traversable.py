from typing import TypeVar, Callable, Protocol, runtime_checkable

import typeclass.applicative as a
import typeclass.foldable as fo
import typeclass.functor as f
import typeclass.monad as m

_T = TypeVar('_T')
_U = TypeVar('_U')
_T_co = TypeVar('_T_co', bound='Traversable', covariant=True)


@runtime_checkable
class Traversable(f.Functor[_T], fo.Foldable[_T], Protocol[_T]):
    def traverse(self, f: Callable[[_T], a.Applicative[_U]]) -> a.Applicative[_T_co]:
        ...

    def sequenceA(self: _T_co) -> a.Applicative[_T_co]:
        import util.functions.common as cm
        return self.traverse(cm.id_)

    def mapM(self, f: Callable[[_T], m.Monad[_U]]) -> m.Monad[_T_co]:
        return self.traverse(f)

    def sequence(self: _T_co) -> m.Monad[_T_co]:
        return self.sequenceA()


__all__ = ['Traversable']
