from typing import TypeVar, Protocol, runtime_checkable

import typeclass.monad as m

_T = TypeVar('_T')
_U = TypeVar('_U')
_M_co = TypeVar('_M_co', bound='MonadFail', covariant=True)

@runtime_checkable
class MonadFail(m.Monad[_T], Protocol[_T]):
    def fail(self, msg: str) -> _M_co:
        ...


__all__ = [
'MonadFail'
]