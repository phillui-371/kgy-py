from typing import TypeVar, Callable, Protocol, runtime_checkable

import typeclass.monoid as m

_T = TypeVar('_T')
_U = TypeVar('_U')
_V = TypeVar('_V')
M = m.Monoid
_B_co = TypeVar('_B_co', bound='Bifoldable', covariant=True)


# https://hackage.haskell.org/package/base-4.14.0.0/docs/Data-Bifoldable.html#t:Bifoldable
@runtime_checkable
class Bifoldable(Protocol[_T, _U]):
    def bi_foldr(self: _B_co, f: Callable[[_T, _V], _V], g: Callable[[_U, _V], _V], init: _V) -> _V:
        ...

    def bi_fold_map(self: _B_co, f: Callable[[_T], M], g: Callable[[_U], M], init: M) -> M:
        """ init should be mempty """
        return self.bi_foldr(lambda x: f(x).mappend, lambda x: g(x).mappend, init)

    def bi_fold(self: _B_co, init: M) -> M:
        import util.functions.common as cm
        return self.bi_fold_map(cm.id_, cm.id_, init)

    def bi_foldl(self, f: Callable[[_V, _T], _V], g: Callable[[_V, _U], _V], init: _V) -> _V:
        ...


__all__ = [
    'Bifoldable'
]
