from typing import TypeVar, Callable, Protocol, runtime_checkable


_T = TypeVar('_T')
_R = TypeVar('_R')
_F_co = TypeVar('_F_co', bound='Functor', covariant=True)


@runtime_checkable
class Functor(Protocol[_T]):
    def fmap(self, fn: Callable[[_T], _R]) -> _F_co: ...

    def __rmul__(self, fn: Callable[[_T], _R]) -> _F_co:
        """
        compatability with PyMonad

        functor.fmap(fn) === fn * functor

        * here is <$> in Haskell, i.e. fn <$> functor
        """
        return self.fmap(fn)

    @classmethod
    def unit(cls, value: _T) -> _F_co: ...

    def replace(self, value: _R) -> _F_co:
        """ (<$) in hs, which is `fmap . const`, act as `value <$ self` """
        import util.functions.common as cm
        return self.fmap(cm.const(value))


__all__ = [
'Functor'
]