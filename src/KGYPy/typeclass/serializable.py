from abc import abstractmethod
from typing import Dict, Union, Protocol, runtime_checkable

SupportedDataType = Union[
    None, bool,
    int, float, complex,
    str, bytes, bytearray,
    tuple, list, set, dict,
]
SerializedReturn = Dict[str, SupportedDataType]


@runtime_checkable
class DeSerializable(Protocol):
    @abstractmethod
    def __setstate__(self, state) -> SerializedReturn:
        ...


@runtime_checkable
class Serializable(Protocol):
    @abstractmethod
    def __getstate__(self):
        ...


@runtime_checkable
class TotalSerializable(Serializable, DeSerializable, Protocol):
    pass

__all__ = ['Serializable', 'SerializedReturn', 'SupportedDataType', 'DeSerializable', 'TotalSerializable']