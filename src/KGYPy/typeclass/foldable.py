import operator
from typing import TypeVar, Protocol, Callable, Iterable, runtime_checkable

import typeclass.monad as monad
import typeclass.monoid as monoid

_T = TypeVar('_T')
_U = TypeVar('_U')
_F_co = TypeVar('_F_co', bound='Foldable', covariant=True)


@runtime_checkable
class Foldable(Iterable[_T], Protocol[_T]):
    def fold_map(self, fn: Callable[[_T], monoid.Monoid]) -> monoid.Monoid:
        ...

    def foldr(self, fn: Callable[[_T, _U], _U], init: _U) -> _U:
        ...

    def foldl(self, fn: Callable[[_U, _T], _U], init: _U) -> _U:
        import util.functions.common as cm
        return self.foldr(lambda g, x: g(fn(x, cm.id_)), init)

    def foldl1(self, fn: Callable[[_T, _T], _T]) -> _T:
        ...  # TODO

    def foldr1(self, fn: Callable[[_T, _T], _T]) -> _T:
        ...  # TODO

    """ Eq """

    def elem(self: _F_co, value: _F_co) -> bool:
        return any(filter(lambda x: operator.eq(value, x), self))

    """ Ord """

    def maximum(self: _F_co) -> _T:
        return self.foldr1(max)

    """ Ord """

    def minimum(self: _F_co) -> _T:
        # TODO
        return self.foldr1(min)

    """ Num """  # TODO

    def sum(self) -> _T:
        return sum(self)

    """ Num """  # TODO

    def product(self) -> _T:
        return self.foldr1(operator.mul)


def sequence_(xs: _F_co) -> monad.Monad[None]:
    if not xs:
        raise StopIteration("xs must not be empty")
    ret = None
    for x in xs:
        ret = x.consume_left(type(x)(None))
    return ret


__all__ = [
    'Foldable', 'sequence_'
]
