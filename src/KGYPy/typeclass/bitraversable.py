from typing import TypeVar, Protocol, runtime_checkable, Callable

import typeclass.applicative as app
import typeclass.bifoldable as bifo
import typeclass.bifunctor as bifu
import util.functions.common as c

_T = TypeVar('_T')
_U = TypeVar('_U')
_B_co = TypeVar('_B_co', bound='Bitraversable', covariant=True)
_A_co = TypeVar('_A_co', bound=app.Applicative, covariant=True)


# https://hackage.haskell.org/package/base-4.14.0.0/docs/Data-Bitraversable.html#t:Bitraversable
@runtime_checkable
class Bitraversable(bifo.Bifoldable[_T, _U], bifu.Bifunctor[_T, _U], Protocol[_T, _U]):
    def bitraverse(self, f: Callable[[_T], _A_co], g: Callable[[_U], _A_co]) -> _A_co:
        return bisequenceA(self.bimap(f, g))


def bisequenceA(t: Bitraversable[_A_co, _A_co]) -> _A_co:
    return bisequence(t)


def bimapM(f: Callable[[_T], _A_co], g: Callable[[_U], _A_co], t: Bitraversable[_T, _U]) -> _A_co:
    return t.bitraverse(f, g)


def bisequence(t: Bitraversable[_A_co, _A_co]) -> _A_co:
    return t.bitraverse(c.id_, c.id_)


__all__ = [
    'Bitraversable', 'bisequenceA', 'bisequence', 'bimapM'
]
