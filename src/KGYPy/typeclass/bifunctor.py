from typing import Protocol, TypeVar, Callable, runtime_checkable

_T = TypeVar('_T')
_U = TypeVar('_U')
_V = TypeVar('_V')
_W = TypeVar('_W')
_B_co = TypeVar('_B_co', bound='Bifunctor', covariant=True)


# https://hackage.haskell.org/package/base-4.14.0.0/docs/Data-Bifunctor.html#t:Bifunctor
@runtime_checkable
class Bifunctor(Protocol[_T, _U]):
    def bimap(self, f: Callable[[_T], _V], g: Callable[[_U], _W]) -> _B_co:
        ...

    def first(self, f: Callable[[_T], _V]) -> _B_co:
        import util.functions.common as cm
        return self.bimap(f, cm.id_)

    def second(self, g: Callable[[_U], _W]) -> _B_co:
        import util.functions.common as cm
        return self.bimap(cm.id_, g)


__all__ = [
    'Bifunctor'
]
