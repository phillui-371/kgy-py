from typing import TypeVar, Protocol, Tuple, Callable, runtime_checkable

import typeclass.monad as monad

_T = TypeVar('_T')
_U = TypeVar('_U')
_V = TypeVar('_V')
_M_co = TypeVar('_M_co', bound='MonadZip', covariant=True)

@runtime_checkable
class MonadZip(monad.Monad[_T], Protocol[_T]):
    def mzip(self, m: _M_co) -> _M_co:
        return self.mzip_with(tuple, self, m)

    def mzip_with(self, f: Callable[[_T, _U], _V], m:_M_co) -> _M_co:
        return monad.liftM(lambda *args, **kwargs: f(*args, **kwargs), self.mzip(m))

    def munzip(self: _M_co) -> Tuple[_M_co, _M_co]:
        import util.functions.collections_ as col
        return monad.liftM(col.fst, self), monad.liftM(col.snd, self)


__all__ = [
'MonadZip'
]