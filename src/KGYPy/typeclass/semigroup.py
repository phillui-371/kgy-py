from typing import Protocol, runtime_checkable, TypeVar

_S_co = TypeVar('_S_co', bound='SemiGroup', covariant=True)

@runtime_checkable
class SemiGroup(Protocol):
    def assoc(self, value: _S_co) -> _S_co:
        ...

__all__ = [
    'SemiGroup'
]