from typing import TypeVar, Protocol, Callable, runtime_checkable

import typeclass.monad as m

_T = TypeVar('_T')
_M_co = TypeVar('_M_co', bound='MonadFix', covariant=True)

@runtime_checkable
class MonadFix(m.Monad[_T], Protocol[_T]):
    @classmethod
    def mfix(cls, fn: Callable[[_T], _M_co]) -> _M_co:
        ...


"""https://www.parsonsmatt.org/2016/10/26/grokking_fix.html"""
# def fix(fn: Callable[[Callable[[_T], _T]], _T]) -> [_T] = Y(fn)
# no idea on implementation

__all__ = [
'MonadFix'
]