from typing import Iterable, Protocol, runtime_checkable, TypeVar

import typeclass.semigroup as s
import basic_type as b

_M_co = TypeVar('_M_co', bound='Monoid', covariant=True)

@runtime_checkable
class Monoid(s.SemiGroup, Protocol):
    @classmethod
    def mempty(cls) -> _M_co:
        return b.Unit

    def mappend(self, value: _M_co) -> _M_co:
        return self.assoc(value)

    def mconcat(self, value: Iterable[_M_co]) -> _M_co:
        import util.functions.common as cm
        return cm.foldr(lambda s, v: s.mappend(v), self.mempty, value)


__all__ = [
    'Monoid'
]