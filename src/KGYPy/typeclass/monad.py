from typing import TypeVar, Callable, Protocol, runtime_checkable

import typeclass.applicative as a
import typeclass.foldable as fo

_T = TypeVar('_T')
_U = TypeVar('_U')
_V = TypeVar('_V')
_M_co = TypeVar('_M_co', bound='Monad', covariant=True)


@runtime_checkable
class Monad(a.Applicative[_T], Protocol[_T]):
    def bind(self, function: Callable[[_T], _M_co]) -> _M_co:
        ...

    def __rshift__(self, function: Callable[[_T], _M_co]):
        """
        alias of (>>=) in Haskell, i think it should also be >>= in python...

        i.e. M >>= (lambda _: Monad(Anything))
        ->   M = M >> (lambda _: Monad(Anything))
        """
        return self.bind(function if callable(function) else (lambda _: function))

    @classmethod
    def return_(cls, value: _T) -> _M_co:
        return cls.unit(value)

    def bind_right(self, v: _M_co) -> _M_co:
        """
        consume first monad and return second monad
        """
        return self >> (lambda _: v)


def liftM(f: Callable[[_T], _U], m: _M_co) -> _M_co:
    return m >> f


def liftM2(f: Callable[[_T, _U], _V], m1: _M_co, m2: _M_co) -> _M_co:
    return m1 >> (lambda x1: m2 >> (lambda x2: f(x1, x2)))

def mapM_(fn: Callable[[_T], _M_co], xs: 'fo.Foldable[_T]') -> _M_co:
    """ iterate xs, apply fn, ignore result """
    if not xs:
        raise StopIteration("xs must not be empty")
    ret = None
    for x in xs:
        ret = fn(x)  # TODO
    return ret


__all__ = [
'Monad', 'liftM', 'liftM2', 'mapM_'
]