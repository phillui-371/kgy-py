from typing import TypeVar, Protocol, runtime_checkable

import typeclass.applicative as a

_T = TypeVar('_T')
_A_co = TypeVar('_A_co', bound='Alternative', covariant=True)


@runtime_checkable
class Alternative(a.Applicative[_T], Protocol[_T]):
    @classmethod
    def empty(cls) -> _A_co:
        ...

    def __or__(self, value: _A_co) -> _A_co:
        ...

    """
    default some and many are not necessary in some cases
    """

    # TODO require a lazy evaluation implementation, or treat as applicative?
    # def some(self) -> Alternative[Sequence[_T]]:
    #     # >= 1
    #     return a.liftA2(operator.add, self, self.many())
    #
    # def many(self) -> Alternative[Sequence[_T]]:
    #     # >= 0
    #     return self.some() | self.pure([])  # PVector

    @classmethod
    def pure(cls, value: _T) -> _A_co:
        return cls.unit(value)


__all__ = [
    'Alternative'
]
