from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Generic, TypeVar, Callable, Iterator

from basic_type import Unit
from typeclass import Monad, MonadFail, Functor, Applicative, Traversable, Alternative, MonadPlus, \
    MonadZip, Monoid, SemiGroup, liftM2

_T = TypeVar('_T')
_U = TypeVar('_U')
_R = TypeVar('_R')
_V = TypeVar('_V')


# TODO util fn like and(), and_then(), unwrap() ...
# TODO MonadFix
@dataclass
class Maybe(
    MonadFail[_T], Traversable[_T], MonadPlus[_T], MonadZip[_T], Alternative[_T], Monoid, Generic[_T], ABC
):
    value: _T

    def getValue(self): return self.value

    @classmethod
    def unit(cls, value: _T) -> Just[_T]:
        return Just(value)

    @property
    @abstractmethod
    def is_just(self) -> bool: pass

    @property
    def is_nothing(self) -> bool: return not self.is_just

    def fail(self, msg: str) -> MonadFail[_U]: return Nothing

    @classmethod
    def mempty(cls) -> Monoid: return Nothing

    def fold_map(self, fn: Callable[[_T], Monoid]) -> Monoid: return maybe(self.mempty, fn, self)

    @classmethod
    def empty(cls) -> Alternative[_T]: return Nothing


class _Nothing(Maybe[type(Unit)]):
    def __init__(self):
        pass

    @property
    def is_just(self) -> bool:
        return False

    def __iter__(self) -> Iterator[_T]:
        raise StopIteration

    def getValue(self):
        raise RuntimeError("Nothing has no value")

    def bind(self, function: Callable[[_T], Monad[_U]]) -> Monad[_U]:
        return self

    def fmap(self, fn: Callable[[_T], _R]) -> Functor[_R]:
        return self

    def amap(self, value: Applicative[_T]) -> Applicative[_U]:
        return self

    def assoc(self, value: Maybe[SemiGroup]) -> Maybe[SemiGroup]:
        return value

    def foldr(self, fn: Callable[[_U, _T], _U], init: _U) -> _U:
        return init

    def traverse(self, f: Callable[[_T], Applicative[_U]]) -> Applicative[_U]: return self.pure(Nothing)

    def __or__(self, value: Alternative[_T]) -> Alternative[_T]: return value

    def __repr__(self): return "Nothing"

    __str__ = __repr__


Nothing = _Nothing()


@dataclass
class Just(Maybe[_T], Generic[_T]):
    @property
    def is_just(self) -> bool:
        return True

    def __iter__(self) -> Iterator[_T]:
        yield self.value

    def bind(self, function: Callable[[_T], Monad[_U]]) -> Monad[_U]:
        try:
            return function(self.getValue())
        except Exception as e:
            return Nothing

    def fmap(self, fn: Callable[[_T], _R]) -> Functor[_R]:
        try:
            return type(self).unit(fn(self.getValue()))
        except Exception:
            return Nothing

    def amap(self, value: Applicative[_T]) -> Applicative[_U]:
        try:
            return type(self).unit(self.getValue()(value))
        except Exception:
            return Nothing

    def assoc(self, value: Maybe[SemiGroup]) -> Maybe[SemiGroup]:
        return Just.unit(self.getValue().assoc(value.getValue()))

    def foldr(self, fn: Callable[[_U, _T], _U], init: _U) -> _U:
        return fn(self.getValue(), init)

    def traverse(self, f: Callable[[_T], Applicative[_U]]) -> Applicative[_U]:
        return Just.unit * f(self.getValue())

    def __or__(self, value: Alternative[_T]) -> Alternative[_T]:
        return self

    def mzip_with(self, f: Callable[[_T, _U], _V], m: MonadZip[_U]) -> MonadZip[_V]:
        return liftM2(f, self, m)


def maybe(u: _U, f: Callable[[_T], _U], m: Maybe[_T]) -> _U:
    if m.is_nothing:
        return u
    elif m.is_just:
        return f(m.getValue())


__all__ = [
'Maybe', 'Nothing', 'Just', 'maybe'
]