from abc import abstractmethod, ABC
from dataclasses import dataclass
from typing import TypeVar, Callable, Union, Any, Generic, Iterator

import basic_type as b
import typeclass.alternative as a
import typeclass.applicative as app
import typeclass.bitraversable as bitra
import typeclass.monad as monad
import typeclass.monoid as monoid
import typeclass.traversable as tr

_T = TypeVar('_T')
_U = TypeVar('_U')
_V = TypeVar('_V')
_E = TypeVar('_E', bound=Exception)


# TODO util fn like and(), and_then(), unwrap() ...
# TODO MonadFix, Bi...
@dataclass(frozen=True)
class Either(
    monad.Monad[Union[_E, _T]], a.Alternative[Union[_E, _T]], tr.Traversable[Union[_E, _T]],
    bitra.Bitraversable[_E, _T],
    Generic[_E, _T], ABC
):
    value: Union[_E, _T]

    def getValue(self) -> Union[_E, _T]:
        return self.value

    @classmethod
    def unit(cls, value: _T) -> 'Right[_T]':
        return Right(value)

    @classmethod
    def empty(cls) -> 'Right[type(b.Unit)]':
        return Right(b.Unit)

    @property
    @abstractmethod
    def is_left(self) -> bool:
        pass

    @property
    def is_right(self) -> bool:
        return not self.is_left


@dataclass(frozen=True)
class Left(Either[_E, Any], Generic[_E]):
    def __iter__(self) -> Iterator[_T]:
        yield self.value

    @property
    def is_left(self) -> bool:
        return True

    def bind(self, _: Callable[[_T], Either[_E, _U]]) -> 'Left[_E]':
        return self

    def __or__(self, value: Either[_E, _U]) -> Either[_E, _U]:
        return value

    def amap(self: 'Left[_E]', _: Either[_E, _T]) -> 'Left[_E]':
        return self

    def fmap(self: 'Left[_E]', _: Callable[[_T], _U]) -> 'Left[_E]':
        return self

    def traverse(self, f: Callable[[_T], app.Applicative[_U]]) -> app.Applicative['Left[_E]']:
        return self.unit(type(self)(self.getValue()))

    def fold_map(self, fn: Callable[[_T], monoid.Monoid]) -> monoid.Monoid:
        return monoid.Monoid.mempty()

    def foldr(self, fn: Callable[[_T, _U], _U], init: _U) -> _U:
        return init


@dataclass(frozen=True)
class Right(Either[Any, _T], Generic[_T]):
    def __iter__(self) -> Iterator[_T]:
        yield self.value

    @property
    def is_left(self) -> bool:
        return False

    def bind(self, function: Callable[[_T], Either[_E, _U]]) -> Either[_E, _U]:
        try:
            return function(self.value)
        except Exception as e:
            return Left(e)

    def __or__(self, other: Either[_E, _U]) -> Either[_E, Union[_T, _U]]:
        return self

    def amap(self: 'Right[Callable[[_T], _U]]', v: Either[_E, _T]) -> Either[_E, _U]:
        try:
            return Right(self.value(v.value))
        except Exception as e:
            return Left(e)

    def fmap(self: 'Right[_T]', fn: Callable[[_T], _U]) -> Either[_E, _U]:
        try:
            return Right(fn(self.value))
        except Exception as e:
            return Left(e)

    def traverse(self, f: Callable[[_T], app.Applicative[_U]]) -> app.Applicative[Either[_E, _U]]:
        return type(self)(f(self.getValue()))

    def fold_map(self, fn: Callable[[_T], monoid.Monoid]) -> monoid.Monoid:
        return fn(self.getValue())

    def foldr(self, fn: Callable[[_T, _U], _U], init: _U) -> _U:
        return fn(self.getValue(), init)


def either(f: Callable[[_E], _V], g: Callable[[_T], _V], e: Either[_E, _T]) -> _V:
    if e.is_left:
        return f(e.getValue())
    elif e.is_right:
        return g(e.getValue())


__all__ = [
    'Either', 'Left', 'Right', 'either'
]
