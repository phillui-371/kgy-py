from dataclasses import dataclass
from typing import TypeVar, Generic, Callable

import util.functions.common as c

_Q = TypeVar('_Q')
_T = TypeVar('_T')
_U = TypeVar('_U')
_V = TypeVar('_V')
_W = TypeVar('_W')

__all__ = []


@dataclass(frozen=True)
class Compose(Generic[_T, _U, _V]):
    fn1: Callable[[_U], _V]
    fn2: Callable[[_T], _U]

    def __call__(self, *args, **kwargs) -> _V:
        return self.fn1(self.fn2(*args, **kwargs))

    def __or__(self, other: Callable[[_Q], _T]) -> Callable[[_Q], _V]:
        return compose_fn(self, other)

    def __ror__(self, other: Callable[[_V], _W]) -> Callable[[_T], _W]:
        return compose_fn(other, self)

    and_then = __ror__
    compose_fn = __or__


__all__.append('Compose')


def compose_fn(fn1: Callable[[_U], _V], fn2: Callable[[_T], _U]) -> Compose[_T, _U, _V]:
    return Compose(fn1, fn2)


__all__.append('compose_fn')

and_then: Callable[
    [Callable[[_T], _U], Callable[[_U], _V]],
    Compose[_U, _T, _V]
] = c.flip(compose_fn)

__all__.append('and_then')
