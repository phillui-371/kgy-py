import functools

import util.curry_ as c


@c.curry
def remove(strs, raw):
    return functools.reduce(
        lambda acc, item: acc.replace(item, ''), strs, raw
    )
__all__ = [
    'remove'
]