import util.curry_ as c

@c.curry
def idx(x, xs): return xs[x]
def fst(xs): return xs[0]
def snd(xs): return xs[1]
head = fst
def tail(xs): return xs[1:]
def init(xs): return xs[:-1]
def last(xs): return xs[-1]

@c.curry
def take_until(f, ls):
    it = iter(ls)
    try:
        while True:
            ret = next(it)
            if f(ret): break
            yield ret
    except StopIteration:
        pass


take_while = c.curry(lambda f, ls: take_until(lambda x: not f(x), ls))


@c.curry
def skip_until(f, ls):
    it = iter(ls)
    try:
        while True:
            ret = next(it)
            if f(ret):
                yield ret
                break
        while True:
            yield next(it)
    except StopIteration:
        pass


skip_while = c.curry(lambda f, ls: skip_until(lambda x: not f(x), ls))


@c.curry
def split_by(f, ls):
    tmp = []  # TODO PVector
    it = iter(ls)
    try:
        while True:
            _ = next(it)
            if not f(_):
                tmp.append(_)
            else:
                yield tmp
                tmp = []
    except StopIteration:
        if tmp:
            yield tmp
        pass

__all__ = [
    'idx',
    'head',
    'tail',
    'init',
    'last',
    'fst',
    'snd',
    'take_while',
    'take_until',
    'skip_until',
    'skip_while',
    'split_by'
]