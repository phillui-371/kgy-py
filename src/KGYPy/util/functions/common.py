import operator
from functools import reduce

import util.curry_ as c


# TODO consider async and multiprocess case


def id_(_): return _


@c.curry
def const(x, _): return x


@c.curry
def flip(f, u, t): return f(t, u)


# TODO
@c.curry
def fix(obj, cond_f, tran_f):
    ret = tran_f(obj)
    while not cond_f(ret):
        ret = tran_f(ret)
    return ret


# Y-combinator
Y = lambda f: (lambda x: x(x))(lambda y: f(lambda *args, **kwargs: y(y)(*args, **kwargs)))


@c.curry
def foldl(f, init, xs):
    # TODO Foldable
    return reduce(f, xs, init)


@c.curry
def foldl1(f, xs):
    # TODO Foldable
    return reduce(f, xs)


@c.curry
def foldr(f, init, xs):
    # TODO Foldable
    return reduce(f, reversed(xs), init)


@c.curry
def foldr1(f, xs):
    # TODO Foldable
    return reduce(f, reversed(xs))


@c.curry
def concat_map(f, xs):
    # TODO Foldable
    return reduce(operator.add, map(f, xs))

__all__ = [
    'id_', 'const', 'flip', 'fix', 'Y', 'foldl', 'foldr', 'foldl1', 'foldr1', 'concat_map'
]