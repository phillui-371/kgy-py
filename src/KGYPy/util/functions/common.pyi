from typing import TypeVar, Callable, Iterable, Sequence, overload, Union

_T = TypeVar('_T')
_U = TypeVar('_U')
_V = TypeVar('_V')

__T_NORMAL_CALLABLE_1_W_TYPE_CHANGE = Callable[[_T], _U]


def id_(_: _T) -> _T: ...


def const(x: _T, _: _U) -> _T: ...
@overload
def const(x: _T) -> Callable[[_U], _T]: ...


def compose(f1: Callable[[_U], _V], f2: Callable[[_T], _U]) -> Callable[[_T], _V]: ...
@overload
def compose(f1: Callable[[_U], _V]) -> Callable[[Callable[[_T], _U]], Callable[[_T], _V]]: ...

def flip(f: Callable[[_T, _U], _V], u: _U, t: _T) -> _V: ...
@overload
def flip(f: Callable[[_T, _U], _V]) -> Union[
    Callable[[_U, _T], _V],
    Callable[[_U], Callable[[_T], _V]]
]: ...
@overload
def flip(f: Callable[[_T, _U], _V], u: _U) -> Callable[[_T], _V]: ...


def fix(obj: _T, cond_f: Callable[[_T], bool], tran_f: Callable[[_T], _T]) -> _T: ...


def Y(
        f: Callable[
            [__T_NORMAL_CALLABLE_1_W_TYPE_CHANGE],
            __T_NORMAL_CALLABLE_1_W_TYPE_CHANGE
        ]
) -> __T_NORMAL_CALLABLE_1_W_TYPE_CHANGE: ...


def foldl(
        f: Callable[[_T, _U], _T],
        init: _T,
        xs: Iterable[_U]
) -> _T: ...


def foldl1(
        f: Callable[[_T, _U], _T],
        xs: Iterable[_U]
) -> _T: ...


def foldr(
        f: Callable[[_U, _T], _T],
        init: _T,
        xs: Iterable[_U]
) -> _T: ...


def foldr1(
        f: Callable[[_U, _T], _T],
        xs: Iterable[_U]
) -> _T: ...


def concat_map(
        f: Callable[[_T], Sequence[_U]],
        xs: Sequence[_T]
) -> Sequence[_U]: ...


def uncurry(
        f: Callable[[_T, _U], _V],
        *args: _T,
        **kwargs: _U
) -> _V: ...


def curry(
        f: Callable[[_T, _U], _V],
) -> Callable[
    [_T],
    Callable[[_U], _V]
]: ...
