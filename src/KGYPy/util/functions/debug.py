import sys
from typing import TypeVar

_T = TypeVar('_T')


def trace(t: _T) -> _T:
    print(t, flush=True, file=sys.stderr)
    return t

__all__ = [
    'trace'
]