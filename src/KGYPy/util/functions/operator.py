import operator

import util.curry_ as c
import util.functions.common as com

# Please use util.functions.common.flip if interchanging param position is required

# math
c_add = c.curry(operator.add)
c_sub = c.curry(operator.sub)
c_mul = c.curry(operator.mul)
c_floordiv = c.curry(operator.floordiv)  # //
c_truediv = c.curry(operator.truediv)  # /
c_mod = c.curry(operator.mod)
c_pow = c.curry(operator.pow)
c_matmul = c.curry(operator.matmul)

# compare
c_lt = c.curry(operator.lt)
c_le = c.curry(operator.le)
c_eq = c.curry(operator.eq)
c_ne = c.curry(operator.ne)
c_ge = c.curry(operator.ge)
c_gt = c.curry(operator.gt)

# logical
c_is = com.flip(operator.is_)  # param position interchanged
c_is_not = com.flip(operator.is_not)  # param position interchanged

# bit
c_and = c.curry(operator.and_)  # &
c_lshift = com.flip(operator.lshift)  # param position interchanged
c_rshift = com.flip(operator.rshift)  # param position interchanged
c_or = c.curry(operator.or_)  # |
c_xor = c.curry(operator.xor)  # ^

# sequence
c_contains = c.curry(operator.contains)
c_count_of = c.curry(operator.countOf)
c_index_of = c.curry(operator.indexOf)
c_delitem = c.curry(operator.delitem)
c_getitem = c.curry(operator.getitem)

# inplace
c_iadd = c.curry(operator.iadd)
c_iand = c.curry(operator.iand)
c_ifloordiv = c.curry(operator.ifloordiv)
c_ilshift = c.curry(operator.ilshift)
c_imod = c.curry(operator.imod)
c_imul = c.curry(operator.imul)
c_imatmul = c.curry(operator.imatmul)
c_ior = c.curry(operator.ior)
c_ipow = c.curry(operator.ipow)
c_irshift = c.curry(operator.irshift)
c_isub = c.curry(operator.isub)
c_itruediv = c.curry(operator.itruediv)
c_ixor = c.curry(operator.ixor)

__all__ = [
    'c_add',
    'c_ior',
    'c_and',
    'c_eq',
    'c_ge',
    'c_gt',
    'c_is',
    'c_le',
    'c_lt',
    'c_mod',
    'c_mul',
    'c_ne',
    'c_or',
    'c_pow',
    'c_sub',
    'c_xor',
    'c_iadd',
    'c_iand',
    'c_imod',
    'c_imul',
    'c_ipow',
    'c_isub',
    'c_ixor',
    'c_is_not',
    'c_lshift',
    'c_delitem',
    'c_matmul',
    'c_rshift',
    'c_ilshift',
    'c_imatmul',
    'c_irshift',
    'c_truediv',
    'c_contains',
    'c_count_of',
    'c_floordiv',
    'c_ifloordiv',
    'c_index_of',
    'c_itruediv',
    'c_getitem'
]