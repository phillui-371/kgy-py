from typing import TypeVar, Callable, overload, Sequence, Container, Union, Hashable, NoReturn, Any, \
    MutableSequence, MutableMapping

_T = TypeVar('_T')
_U = TypeVar('_U')
_T_OR_U = Union[_T, _U]

@overload
def c_add(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_add(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_sub(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_sub(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_mul(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_mul(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_floordiv(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_floordiv(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_truediv(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_truediv(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_mod(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_mod(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_pow(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_pow(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_lt(t1: _T, t2: _T) -> bool: pass
@overload
def c_lt(t1: _T) -> Callable[[_T], bool]: pass
@overload
def c_le(t1: _T, t2: _T) -> bool: pass
@overload
def c_le(t1: _T) -> Callable[[_T], bool]: pass
@overload
def c_eq(t1: _T, t2: _T) -> bool: pass
@overload
def c_eq(t1: _T) -> Callable[[_T], bool]: pass
@overload
def c_ne(t1: _T, t2: _T) -> bool: pass
@overload
def c_ne(t1: _T) -> Callable[[_T], bool]: pass
@overload
def c_ge(t1: _T, t2: _T) -> bool: pass
@overload
def c_ge(t1: _T) -> Callable[[_T], bool]: pass
@overload
def c_gt(t1: _T, t2: _T) -> bool: pass
@overload
def c_gt(t1: _T) -> Callable[[_T], bool]: pass
@overload
def c_is(t1: _T, t2: Any) -> bool: pass
@overload
def c_is(t1: _T) -> Callable[[Any], bool]: pass
@overload
def c_is_not(t1: _T, t2: Any) -> bool: pass
@overload
def c_is_not(t1: _T) -> Callable[[Any], bool]: pass
@overload
def c_contains(t1: Container[_T], t2: _T) -> bool: pass
@overload
def c_contains(t1: Container[_T]) -> Callable[[_T], bool]: pass
@overload
def c_count_of(t1: Sequence[_T], t2: _T) -> int: pass
@overload
def c_count_of(t1: Sequence[_T]) -> Callable[[_T], int]: pass
@overload
def c_and(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_and(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_lshift(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_lshift(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_rshift(t1: _T, t2: _U) -> _T_OR_U: pass
@overload
def c_rshift(t1: _T) -> Callable[[_U], _T_OR_U]: pass
@overload
def c_or(t1: _T, t2: _T) -> _T: pass
@overload
def c_or(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_xor(t1: _T, t2: _T) -> _T: pass
@overload
def c_xor(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_matmul(t1: _T, t2: _T) -> _T: pass
@overload
def c_matmul(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_pow(t1: _T, t2: _T) -> _T: pass
@overload
def c_pow(t1: _T) -> Callable[[_T], _T]: pass
_T_GET_ITEM = Union[MutableMapping[Hashable, _T], MutableSequence[_T]]
@overload
def c_delitem(t1: _T_GET_ITEM, t2: Hashable) -> NoReturn: pass
@overload
def c_delitem(t1: _T_GET_ITEM) -> Callable[[Hashable], NoReturn]: pass
@overload
def c_getitem(t1: _T_GET_ITEM, t2: Hashable) -> _T: pass
@overload
def c_getitem(t1: _T_GET_ITEM) -> Callable[[Hashable], _T]: pass
@overload
def c_index_of(t1: Sequence[_T], t2: _T) -> int: pass
@overload
def c_index_of(t1: Sequence[_T]) -> Callable[[_T], int]: pass
@overload
def c_iadd(t1: _T, t2: _T) -> _T: pass
@overload
def c_iadd(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_iand(t1: _T, t2: _T) -> _T: pass
@overload
def c_iand(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_ifloordiv(t1: _T, t2: _T) -> _T: pass
@overload
def c_ifloordiv(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_ilshift(t1: _T, t2: _T) -> _T: pass
@overload
def c_ilshift(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_irshift(t1: _T, t2: _T) -> _T: pass
@overload
def c_irshift(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_imod(t1: _T, t2: _T) -> _T: pass
@overload
def c_imod(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_imul(t1: _T, t2: _T) -> _T: pass
@overload
def c_imul(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_imatmul(t1: _T, t2: _T) -> _T: pass
@overload
def c_imatmul(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_ior(t1: _T, t2: _T) -> _T: pass
@overload
def c_ior(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_ipow(t1: _T, t2: _T) -> _T: pass
@overload
def c_ipow(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_isub(t1: _T, t2: _T) -> _T: pass
@overload
def c_isub(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_itruediv(t1: _T, t2: _T) -> _T: pass
@overload
def c_itruediv(t1: _T) -> Callable[[_T], _T]: pass
@overload
def c_ixor(t1: _T, t2: _T) -> _T: pass
@overload
def c_ixor(t1: _T) -> Callable[[_T], _T]: pass