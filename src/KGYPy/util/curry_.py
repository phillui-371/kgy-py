import inspect
from collections import OrderedDict
from typing import Any, Sequence, Union, Tuple, Callable, TypeVar

import typeclass.serializable as s

_T = TypeVar('_T')
_R = TypeVar('_R')

# TODO mp/async/lazy/generator?
class __Curry(s.TotalSerializable):
    def __init__(self, _f, *_args, **_kwargs):
        """
        curry core

        *args and **kwargs is not allowed

        will return result or new curry object everytime invoked

        the invoke mechanism is to fill args into kwargs empty position, then invoke via f(**kwargs)
        BUT if function is builtin, no kwargs will be used, will only invoke f(*args)

        if no params at all, curried function will act as lazy evaluated function

        will check kwargs key valid or not, non valid key will be ignore
        """
        from basic_type import Unit
        result = self.__is_valid_fn(_f)
        if result.is_left:
            raise result.getValue()
        self.__f = _f
        self.__params = self.__extract_param(_f)
        self.__params: Sequence[Tuple[str, Union[Unit, Any]]] = list({
            **self.__params,
            **{x: y for x, y in _kwargs.items() if x in self.__params}
        }.items())
        self.__args: Sequence[Any] = [*_args]
        self.__filled_count = sum(x != Unit for x in map(lambda x: x[1], self.__params)) + len(self.__args)

    def __str__(self):
        from basic_type import Unit
        args = ",".join(map(str, self.__args))
        kwargs = ",".join(map(lambda x: "=".join(x),
                              ((str(x), str(y) if y != Unit else "{Unassigned}") for x, y in self.__params)))
        return f'{self.__f.__name__}(' + (args if args else '') + (
            f',{kwargs}' if kwargs and args else kwargs if kwargs else '') + ')'

    __repr__ = __str__

    @property
    def __max_count(self) -> int:
        return len(self.__params)

    @staticmethod
    def __is_valid_fn(f):
        from dataclass import Left, Right
        if not callable(f):
            return Left(ValueError(
                f"{f} is not callable."
            ))

        if inspect.isfunction(f) or inspect.isbuiltin(f):
            # builtin, function and lambda
            sig = inspect.signature(f)
            params = sig.parameters
            if any(map(
                    lambda x: x[1].kind in (inspect.Parameter.VAR_POSITIONAL, inspect.Parameter.VAR_KEYWORD),
                    params.items())
            ):
                return Left(ValueError(
                    f"Function {f.__name__} has *args/**kwargs which cannot be handled by curry. Please check again."
                ))
        elif inspect.isclass(f):
            # need special handling for builtin cls like map
            # primitive type like int will not be accepted
            if f in {map, filter}:
                pass
            if f in {int, bool, bytes,bytearray,complex,dict,float, frozenset, set, list, range, str, memoryview, slice, enumerate}:
                return Left(TypeError(f'builtin class {f} is not supported by curry.'))
            if f is reversed:
                return Left(TypeError('single param function `reversed` should not be curried. Use reverse as function object'))
            if f is zip:
                return Left(TypeError('`zip` supports varargs which is not supported by curry'))
        return Right.empty()

    @staticmethod
    def __extract_param(f):
        from basic_type import Unit
        if f in {map, filter}:
            return OrderedDict([
                ('fn', Unit),
                ('it', Unit)
            ])
        return OrderedDict(map(
            lambda x: (x[0], x[1].default if x[1].default is not inspect.Parameter.empty else Unit),
            inspect.signature(f).parameters.items()
        ))

    def __call__(self, *args, **kwargs):
        from basic_type import Unit
        temp_args = [*self.__args, *args]
        temp_params = list(map(
            lambda x: (x[0], x[1] if x[0] not in kwargs or kwargs[x[0]] == Unit else kwargs[x[0]]),
            self.__params
        ))
        from util.functions.collections_ import snd, fst
        count = len(temp_args) + len(list(filter(lambda x: x != Unit, map(snd, temp_params))))
        if count > self.__max_count:
            raise ValueError(
                f"Too many params provided. fn: {self.__f.__name__}, args: {temp_args}, kwargs: {temp_params}"
            )
        if count == self.__max_count:
            # builtin does not support kwargs
            if inspect.isbuiltin(self.__f) or self.__f in {map, filter}:
                if temp_params:
                    temp_args.extend(y for x, y in temp_params if y != Unit)
                return self.__f(*temp_args)
            # normal function invoke
            if temp_args:
                idx = 0
                for i in range(len(temp_params)):
                    if snd(temp_params[i]) == Unit:
                        temp_params[i] = (fst(temp_params[i]), temp_args[idx])
                        idx += 1
            return self.__f(**dict(temp_params))
        return type(self)(self.__f, *temp_args, **dict(temp_params))

    def __getstate__(self):
        """
        require dill
        """
        pass

    def __setstate__(self, state):
        """
        require dill
        """
        pass


curry = __Curry


__all__ = ['curry']