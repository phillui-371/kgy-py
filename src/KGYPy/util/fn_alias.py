from typing import TypeVar, Union

import util.curry_ as c

_T = TypeVar('_T')
_R = TypeVar('_R')


@c.curry
def if_(cond: bool, then: _T, else_: _R) -> Union[_T, _R]:
    return then if cond else else_


@c.curry
def if_then(cond: bool, then: _T):
    from KGYPy import Nothing
    return if_(cond, then, Nothing)

__all__ = ['if_', 'if_then']