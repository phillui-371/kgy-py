from typing import Mapping, TypeVar, Callable, Any

import typeclass.serializable as s

_T = TypeVar('_T')
_U = TypeVar('_U')
_R = TypeVar('_R')

class __Curry(s.TotalSerializable):
    def __init__(self, f: Callable[[_T], _R], *args: Any, **kwargs: Any):
        ...

    def __getstate__(self) -> Mapping:
        ...

    def __setstate__(self, state: Mapping):
        ...


curry: Callable[
    [Callable[[_T, _U], _R]],
    Callable[[_T], Callable[[_U], _R]]
]
